﻿using System;
using System.Collections.Generic;
using System.Linq;
using ParityBitCount.Services.ActionableEntityRunner;
using ParityBitCount.Services.Actions;
using ParityBitCount.Services.Conditions;
using ParityBitCount.Services.IteratorMutators;
using ParityBitCount.Solution;

namespace ParityBitCount
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var inputArray = new int[] {1, -3, 4, 7, -2, 9, -10}; // {1, 20, 8, -2, -3}
             
            var result = BadAndNaiveApproach(inputArray);
            var result2 = BetterApproach(inputArray);
            Console.WriteLine(result);
            Console.WriteLine(result2);
        }

        static int BadAndNaiveApproach(IEnumerable<int> inputArray)
        {
            // [1,-1,4,7,-2,9]
            // [4,7,-2,9]
            var max = inputArray.First();
            for (var i=0;i<inputArray.Count(); i++)
            {
                var subArraySum = 0;
                for (var j = i; j < inputArray.Count(); j++)
                {
                    subArraySum += inputArray.ElementAt(j);
                    if (subArraySum > max)
                    {
                        max = subArraySum;
                    }
                }

                subArraySum = 0;
            }

            return max;

        }

        static int BetterApproach(IEnumerable<int> inputArray)
        {
            //1, -3, 4, 7, -2, 9, -10
            var sum = 0;
            var max = 0;
            
            for (var i = 0; i < inputArray.Count() - 1; i++)
            {
                if (sum + inputArray.ElementAt(i+1) < 0)
                {
                    i++;
                    sum = inputArray.ElementAt(i+1);
                }
                else
                {
                    sum += inputArray.ElementAt(i + 1);
                    if (sum > max)
                    {
                        max = sum;
                    }
                }
            }

            return max;
        }
    }
}
